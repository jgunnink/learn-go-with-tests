package hello

import (
	"fmt"
	"io"
	"os"
)

func Greeting() {
	Greet(os.Stdout, "Billie")
}

func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}
