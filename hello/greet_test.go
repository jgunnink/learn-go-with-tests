package hello

import (
	"bytes"
	"testing"
)

func TestGreet(t *testing.T) {
	buffer := bytes.Buffer{}
	Greet(&buffer, "JK")

	got := buffer.String()
	want := "Hello, JK"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
